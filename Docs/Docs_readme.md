This folder will eventually contain project documentation covering:

How to use the project/product
How to manufacture the project/product
How to maintain and further develop/contribute to the project/product

It is also a useful location to store datasheets related to any parts or components used in the design
